package INF102.lab6.cheapFlights;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheapestFlight implements ICheapestFlight {

    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();

        // Lager HashMap for å lagre hjørner basert på by
        Map<Integer, City> cityMap = new HashMap<>();

        // Legger til hjørner og kanter til grafen basert på fly
        for (Flight flight : flights) {
            City start = getOrCreateCity(cityMap, flight.start);
            City mål = getOrCreateCity(cityMap, flight.destination);

            int cost = flight.cost;

            // Legger til kant fra start til mål med pris som vekt 
            graph.addEdge(start, mål, cost);
        }

        return graph;
    }

    // Hjelpe-metode for å hente eller lage en by 
    private City getOrCreateCity(Map<Integer, City> cityMap, City city) {
        if (!cityMap.containsKey(city.cityID)) {
            cityMap.put(city.cityID, city);
        }
        return cityMap.get(city.cityID);
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        throw new UnsupportedOperationException("Implement me :)");
    }
}